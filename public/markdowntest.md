# Titre 1
## Titre 2
### Titre 3
#### Titre 4
##### Titre 5
###### Titre 6

### Paragraphe

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue.<br>
Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.

### Gras

**Ceci est du texte en gras**

### Italique

*Ceci est du texte en italique*

### Retours à la ligne
<br>
<br>
Il a deux retours à la lige avant ce paragraphes

### Listes ordonnées

1. élément 1
  - sous-élément<br>
Salut je m'appel Jean Millot
  - sous-élément
2. élément 2
3. élément 3

### Listes non ordonnées

- élément 1
- élément 2
- élément 3

### Citations
> Ceci est une citation

### Lignes horizontales

***

### Liens

[Lien vers brahms](https://brahms.ircam.fr/)

### Tableau

| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |

### Bloque de code

```
curve C
 @action := { level $a },
 @grain := 0.1
 {
        $a
        {
               { 0 } @type "linear"
             2 { 2 } @type "linear"
             8 { 4 }
        }
 }
```

### Images
![Figure 1: 300x300]("http://via.placeholder.com/300x300")

![Figure 2: 1200x1200]("http://via.placeholder.com/1200x1200")

![Figure 3: 300x1200]("http://via.placeholder.com/300x1200")

![Figure 4: 1200x300]("http://via.placeholder.com/1200x300")
