const app = Vue.createApp({
	data(){
		return{
			style : "assets/sidney/sidney1.css",
			styles : ["assets/sidney/sidney1.css",
			"assets/sidney/sidney2.css",
			"assets/sidney/sidney3.css",
			"assets/sidney/sidney4.css",
			"assets/sidney/sidney5.css",
			"assets/github/github-markdown.css",
			"assets/modest/modest.css",
			"assets/splendor/splendor.css",
			"assets/cssZG214.css"]
		};
	},
	methods:{
		styleSwitch(n){
			if (n===1)
				this.style = "assets/sidney/sidney1.css";
			else if (n===2)
				this.style = "assets/sidney/sidney2.css";
			else if (n===3)
				this.style = "assets/sidney/sidney3.css";
			else if (n===4)
				this.style = "assets/sidney/sidney4.css";
			else if (n===5)
				this.style = "assets/github/github-markdown.css";
			else if (n===6)
				this.style = "assets/modest/modest.css";
			else if (n===7)
				this.style = "assets/splendor/splendor.css";
			else if (n===8)
				this.style = "assets/cssZG214.css";
		},
		styleSwitch2(s){
			this.style = s;
		}
	}
});

app.mount('#styleselection');
